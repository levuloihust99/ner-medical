import torch
from torch.utils.data import DataLoader, TensorDataset
from transformers import (
    BertTokenizer,
    PhobertTokenizer,
    ElectraTokenizer,
    RobertaForTokenClassification
)

from models import (
    BertPosTagger,
    PhoBertPosTagger,
)
from seqeval.metrics import classification_report
from tensorflow.keras.preprocessing.sequence import pad_sequences

import logging
import pickle
import json
import time
import datetime
import argparse
import os
from tqdm import tqdm

from data_utils import taggedseq2doccano

parser = argparse.ArgumentParser()
parser.add_argument('--model-path',
                    help='Path to model to be evaluated')
args = parser.parse_args()


def format_time(elapsed):
    elapsed_rounded = int(round(elapsed))
    return str(datetime.timedelta(seconds=elapsed_rounded))


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MODEL_PATH = args.model_path
MAX_SEQUENCE_LENGTH = 256

with open(os.path.join(MODEL_PATH, 'label_mapping.json'), 'r') as reader:
    label_mapping = json.load(reader)
inverse_label_mapping = {v: k for k, v in label_mapping.items()}

if MODEL_PATH.find('vinai/phobert-base') > -1:
    tokenizer = PhobertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    model = RobertaForTokenClassification.from_pretrained(MODEL_PATH)
else:
    with open(os.path.join(MODEL_PATH, "store_dict.json"), 'r') as reader:
        model_args = json.load(reader)
    model_args = argparse.Namespace(**model_args)
    tokenizer = BertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    model = BertPosTagger.from_pretrained(MODEL_PATH, args=model_args)

model.eval()

"""
Setup CUDA
"""
if torch.cuda.is_available():
    model.cuda()
    device = torch.device("cuda")
    logger.info('There are %d GPU(s) available.' % torch.cuda.device_count())
    logger.info('We will use the GPU:{}, {}'.format(torch.cuda.get_device_name(0), torch.cuda.get_device_capability(0)))
else:
    logger.info('No GPU available, using the CPU instead.')
    device = torch.device("cpu")


with open("text_to_predict.txt") as f:
    input_texts = f.read()
input_texts = input_texts.split('\n')
if input_texts[-1] == '':
    input_texts.pop()

input_ids = []
label_ids = []
for sent in input_texts:
    encoded_sent = tokenizer.encode(sent, add_special_tokens=True)
    input_ids.append(encoded_sent)

# Padding to max length
padded_input_ids = pad_sequences(input_ids, maxlen=MAX_SEQUENCE_LENGTH,
                               dtype='long', value=0,
                               truncating='post', padding='post')

input_masks = []
for sent in padded_input_ids:
    attention_mask = [int(token > 0) for token in sent]
    input_masks.append(attention_mask)

padded_input_ids = torch.tensor(padded_input_ids, dtype=torch.long)
input_masks = torch.tensor(input_masks, dtype=torch.long)

dataset = TensorDataset(padded_input_ids, input_masks)
dataloader = DataLoader(dataset, batch_size=32, shuffle=False)

# Run prediction
start_time = time.time()

tagged_sequences = []
offset = 0
for batch in tqdm(dataloader, total=len(dataloader)):
    batch = tuple(t.to(device) for t in batch)
    batch_input_ids, batch_input_masks = batch

    with torch.no_grad():
        outputs = model(
            input_ids=batch_input_ids,
            attention_mask=batch_input_masks,
        )
    
    logits = outputs[0]
    predictions = torch.argmax(logits, dim=-1)

    for i, pred in enumerate(predictions):

        orig_input_ids = input_ids[offset + i][1:-1]
        pred = pred[1 : 1 + len(orig_input_ids)]
        pred = [inverse_label_mapping[idx.item()] for idx in pred]
        orig_input_text = tokenizer.decode(orig_input_ids)
        orig_input_words = orig_input_text.split()
        
        tagged_seq = []
        inner_offset = 0
        for word in orig_input_words:
            tokens = tokenizer.tokenize(word)
            # assert all(pred[inner_offset] == pred[inner_offset + j] 
            #            for j in range(len(tokens)))
            tagged_seq.append((word, pred[inner_offset]))
            inner_offset += len(tokens)
        
        tagged_sequences.append(tagged_seq)

    offset += len(batch)

doccano_dataset, _ = taggedseq2doccano(tagged_sequences)
doccano_repr = '\n'.join(doccano_dataset)
with open("chan_doan_v2_silver.jsonl", "w") as writer:
    writer.write(doccano_repr)