import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import get_linear_schedule_with_warmup
from transformers import AdamW
from transformers import (
    BertTokenizer,
    PhobertTokenizer,
    ElectraTokenizer,
    ElectraConfig
)

from seqeval.metrics import classification_report
from seqeval.metrics import precision_score, recall_score, f1_score, accuracy_score

import argparse
import os
import time
import datetime
import random
import pickle
import logging as sys_log
from tqdm import tqdm
import json
import string

import utils
import models
import yaml

RUN_ID = ''.join(random.choice(string.digits + string.ascii_uppercase) for _ in range(16))

# seed_val = 42
# random.seed(seed_val)
# np.random.seed(seed_val)
# torch.manual_seed(seed_val)
# torch.cuda.manual_seed_all(seed_val)
# torch.cuda.manual_seed(seed_val)
# torch.backends.cudnn.deterministic = True
# torch.backends.cudnn.benchmark = False

MAX_SEQUENCE_LENGTH = 256

parser = argparse.ArgumentParser()

parser.add_argument('--model-path', default='pretrained/vinai/phobert-base',
                    help='Pretrained model to be used, default to `vinai/phobert-base`')
parser.add_argument('--model-save', default='checkpoints',
                    help='Location to save the model')
parser.add_argument('--vlsp', type=bool, default=False)
parser.add_argument('--batch-size', default=16, type=int)
parser.add_argument('--val-batch-size', type=int, default=32)
parser.add_argument('--dropout-prob', default=0.1, type=float)
parser.add_argument('--use-crf', default=False, type=bool)
parser.add_argument("--weight-decay", default=0.01, type=float,
                    help="Weight decay if we apply some.")
parser.add_argument("--adam-epsilon", default=1e-8, type=float,
                    help="Epsilon for Adam optimizer.")
parser.add_argument("--learning-rate",
                    default=5e-5,
                    type=float,
                    help="The initial learning rate for Adam.")
parser.add_argument("--epochs",
                    default=10,
                    type=int,
                    help="Total number of training epochs to perform.")
parser.add_argument('--gradient-accumulation-steps',
                    type=int,
                    default=1,
                    help="Number of updates steps to accumulate before performing a backward/update pass.")
parser.add_argument("--warmup_proportion",
                    default=0.1,
                    type=float,
                    help="Proportion of training to perform linear learning rate warmup for. ")
parser.add_argument("--max-steps", default=-1, type=int,
                    help="If > 0: set total number of training steps to perform. Override num_train_epochs.")
parser.add_argument("--warmup-steps", default=0, type=int, help="Linear warmup over warmup_steps.")
parser.add_argument("--save-pretrained", default=False, type=str, help="Whether to save checkpoints")
parser.add_argument("--debug", type=eval, default=False)
# change these parameters
parser.add_argument("--pool-type", default="concat", type=str, choices=['concat', 'average'])
parser.add_argument("--ignore-index", default=-100, type=int)
parser.add_argument("--add-special-tokens", type=eval, default=True)
parser.add_argument("--max-grad-norm", type=float, default=1.0)
parser.add_argument("--use-dice-loss", type=eval, default=True)
parser.add_argument('--num-hidden-layer', default=1, type=int)
parser.add_argument('--use-word-segmenter', type=eval, default=True)

args = parser.parse_args()

MODEL_PATH = args.model_path
MODEL_SUB_PATH = os.sep.join(MODEL_PATH.split(os.sep)[1:])


STORE_DIR = os.path.join(args.model_save, "vlsp" if args.vlsp else "medical", MODEL_SUB_PATH, RUN_ID)
if not os.path.exists(STORE_DIR):
    os.makedirs(STORE_DIR)

with open(os.path.join(STORE_DIR, "config.json"), "w") as writer:
    json.dump(args.__dict__, writer, indent=4, ensure_ascii=False)

sys_log.basicConfig(level=sys_log.DEBUG)
logger = sys_log.getLogger(__name__)
logger.setLevel(sys_log.INFO)
log_file = os.path.join(STORE_DIR, "track.log")

file_handler = sys_log.FileHandler(log_file)
file_handler.setLevel(sys_log.INFO)
formatter = sys_log.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

"""
Setup CUDA
"""
if torch.cuda.is_available():
    device = torch.device("cuda")
    logger.info('There are %d GPU(s) available.' % torch.cuda.device_count())
    logger.info('We will use the GPU:{}, {}'.format(torch.cuda.get_device_name(0), torch.cuda.get_device_capability(0)))
else:
    logger.info('No GPU available, using the CPU instead.')
    device = torch.device("cpu")


def main():
    """
    READ NER DATA
    """
    if args.vlsp:
        train_df = pd.read_csv("data/VLSP-2018-NER/chituoi/ner/train_processed.csv")
        dev_df = pd.read_csv("data/VLSP-2018-NER/chituoi/ner/dev_processed.csv")

        if args.use_word_segmenter:
            train_tokens = train_df.segmented_tokens
            train_labels = train_df.segmented_labels
            dev_tokens   = dev_df.segmented_tokens
            dev_labels   = dev_df.segmented_labels
        else:
            train_tokens = train_df.tokens
            train_labels = train_df.labels
            dev_tokens   = dev_df.tokens
            dev_labels   = dev_df.labels

        train_sentences = [list(zip(eval(tokens), eval(labels))) for tokens, labels in zip(train_tokens, train_labels)]
        assert len(train_tokens) == len(train_tokens)
        assert all([len(eval(tokens)) == len(eval(labels)) for tokens, labels in zip(train_tokens, train_labels)])

        dev_sentences = [list(zip(eval(tokens), eval(labels))) for tokens, labels in zip(dev_tokens, dev_labels)]
        assert len(dev_tokens) == len(dev_labels)
        assert all([len(eval(tokens)) == len(eval(labels)) for tokens, labels in zip(dev_tokens, dev_labels)])

    else:
        # <ner-medical data, prepocessed>
        if args.use_word_segmenter:
            train_data_path = "data/medical/train_sentences_segmented.pkl"
            dev_data_path   = "data/medical/dev_sentences_segmented.pkl"
        else:
            train_data_path = "data/medical/train_sentences.pkl"
            dev_data_path   = "data/medical/dev_sentences.pkl"
        
        with open(train_data_path, 'rb') as f_train, \
                open(dev_data_path, 'rb') as f_dev:
            train_sentences = pickle.load(f_train)
            dev_sentences = pickle.load(f_dev)
        # </ner-medical data, preprocessed>

    tags = set([item for sublist in train_sentences + dev_sentences for _, item in sublist])

    print("Tags", tags)
    tag2int = {}
    int2tag = {}
    for i, tag in enumerate(sorted(tags)):
        tag2int[tag] = i + 1
        int2tag[i + 1] = tag

    tag2int['-PAD-'] = 0
    int2tag[0] = '-PAD-'

    with open(os.path.join(STORE_DIR, "label_mappings.json"), "w") as writer:
        json.dump(tag2int, writer, indent=4, ensure_ascii=False)

    # Special character for the tags
    n_tags = len(tag2int)
    logger.info('Total tags:{}'.format(n_tags))
    logger.info(tag2int)
    

    logger.info('Max sentence length:{}'.format(len(max(train_sentences + dev_sentences, key=len))))

    def split(sentences, max_window):
        new = []
        for data in sentences:
            new.append(([data[x:x + max_window] for x in range(0, len(data), max_window)]))

        new = [val for sublist in new for val in sublist]
        return new

    train_sentences = split(train_sentences, MAX_SEQUENCE_LENGTH)
    dev_sentences = split(dev_sentences, MAX_SEQUENCE_LENGTH)

    logger.info(len(max(train_sentences + dev_sentences, key=len)))

    """
    Tokenizer
    """
    # Load the BERT tokenizer.
    logger.info('Loading BERT tokenizer...')
    if MODEL_PATH.find('NlpHUST/vibert4news-base-cased') > -1:
        tokenizer = BertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    elif MODEL_PATH.find('NlpHUST/vi-electra-base') > -1:
        tokenizer = ElectraTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    elif MODEL_PATH.find('vinai/phobert-base') > -1:  #
        tokenizer = PhobertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    else:
        tokenizer = BertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)

    # if args.use_word_segmenter:
    #     from data_utils import segment_word
    #     if MODEL_PATH.find("vinai/phobert-base") > -1:
    #         from vncorenlp import VnCoreNLP
    #         segmenter = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')
    #     elif MODEL_PATH.find("NlpHUST/vibert4news-base-cased"):
    #         from vncorenlp import VnCoreNLP
    #         segmenter = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')

    #     train_sentences = segment_word(train_sentences, segmenter)
    #     dev_sentences = segment_word(dev_sentences, segmenter)
        
    train_text = utils.text_sequence(train_sentences)
    dev_text = utils.text_sequence(dev_sentences)

    train_label = utils.tag_sequence(train_sentences, tag2int)
    dev_label = utils.tag_sequence(dev_sentences, tag2int)

    """
    Data Loader
    """
    def convert_to_data_loader(train_text, train_label, mode="train"):
        input_ids = []
        labels_ids = []
        for i in range(len(train_text)):
            sentence_splitted = train_text[i]
            sentence = " ".join(sentence_splitted)
            tokens = tokenizer.tokenize(sentence)
            lbs = []

            for j, word in enumerate(sentence_splitted):
                word_tokens = tokenizer.tokenize(word)
                lb = train_label[i][j]
                lbs.extend([lb] * len(word_tokens))

            # debug
            assert len(lbs) == len(tokens)

            if args.add_special_tokens:
                if len(tokens) > MAX_SEQUENCE_LENGTH - 2:
                    tokens = tokens[:(MAX_SEQUENCE_LENGTH - 2)]
                    lbs = lbs[:(MAX_SEQUENCE_LENGTH - 2)]
                tokens = [tokenizer.cls_token] + tokens + [tokenizer.sep_token]
                lbs = [tag2int['-PAD-']] + lbs + [tag2int['-PAD-']]

                padding_length = MAX_SEQUENCE_LENGTH - len(tokens)
                tokens = tokens + [tokenizer.pad_token] * padding_length
                lbs = lbs + [tag2int['-PAD-']] * padding_length
                ids = tokenizer.convert_tokens_to_ids(tokens)
            else:
                if len(tokens) > MAX_SEQUENCE_LENGTH:
                    tokens = tokens[:(MAX_SEQUENCE_LENGTH)]
                    lbs = lbs[:(MAX_SEQUENCE_LENGTH)]

                padding_length = MAX_SEQUENCE_LENGTH - len(tokens)
                tokens = tokens + [tokenizer.pad_token] * padding_length
                lbs = lbs + [tag2int['-PAD-']] * padding_length
                ids = tokenizer.convert_tokens_to_ids(tokens)
            
            input_ids.append(ids)
            labels_ids.append(lbs)

        attention_masks = []
        for sent in input_ids:
            mask = [int(token_id != tokenizer.pad_token_id) for token_id in sent]
            attention_masks.append(mask)
        
        input_ids = torch.tensor(input_ids, dtype=torch.long)
        labels_ids = torch.tensor(labels_ids, dtype=torch.long)
        attention_masks = torch.tensor(attention_masks, dtype=torch.long)

        if mode == "train":
            # Create the DataLoader for our training set.
            data = TensorDataset(input_ids, attention_masks, labels_ids)
            dataloader = DataLoader(data, shuffle=True, batch_size=args.batch_size)
        else:
            # Create the DataLoader for our validation set.
            data = TensorDataset(input_ids, attention_masks, labels_ids)
            sampler = SequentialSampler(data)
            dataloader = DataLoader(data, sampler=sampler, batch_size=args.val_batch_size)

        return dataloader

    train_dataloader = convert_to_data_loader(train_text, train_label)
    val_dataloader = convert_to_data_loader(dev_text, dev_label, mode="val")

    """
    Loading Pretrain Bert4news model
    """
    # vibert4news-base-cased
    model = None
    if MODEL_PATH.find('NlpHUST/vibert4news-base-cased') > -1:
        model = models.BertPosTagger.from_pretrained(
            MODEL_PATH,
            num_labels=len(tag2int),
            output_attentions=False,
            output_hidden_states=True,
            args=args,
        )

    # vi-electra-base
    elif MODEL_PATH.find('NlpHUST/vi-electra-base') > -1:
        config = ElectraConfig.from_pretrained(MODEL_PATH, finetuning_task='ner', num_labels=len(tag2int),
                                                output_attentions=False, output_hidden_states=True, )
        model = models.BertPosTaggerElectra.from_pretrained(
            MODEL_PATH,
            config=config,
            args=args,
        )

    # PhoBert
    elif MODEL_PATH.find('vinai/phobert-base') > -1:
        model = models.PhoBertPosTagger.from_pretrained(
            MODEL_PATH,
            num_labels=len(tag2int),
            output_attentions=False,
            output_hidden_states=True,
            args=args
        )

    else:
        model = models.BertPosTagger.from_pretrained(
            MODEL_PATH,
            num_labels=len(tag2int),
            output_attentions=False,
            output_hidden_states=True,
            args=args,
        )

    # Tell pytorch to run this model on the GPU.
    if torch.cuda.is_available():
        model.cuda()

    """
    Hyper Parameters
    """
    no_decay = ['bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)],
         'weight_decay': args.weight_decay},
        {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)],
         'weight_decay': 0.0}
    ]
    optimizer = AdamW(optimizer_grouped_parameters, lr=args.learning_rate, eps=args.adam_epsilon)
    total_steps = len(train_dataloader) * args.epochs
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=args.warmup_steps,
                                                num_training_steps=total_steps)

    """
    Training HERE
    """
    best_result = 0
    best_report = None
    for epoch_i in range(0, args.epochs):
        # logger.info("")
        logger.info(' Epoch {:} / {:}'.format(epoch_i + 1, args.epochs))
        # logger.info('Training...')

        t0 = time.time()
        total_loss = 0
        model.train()

        for step, batch in tqdm(enumerate(train_dataloader), total=len(train_dataloader)):
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)

            outputs = model(input_ids=b_input_ids,
                            token_type_ids=None,
                            attention_mask=b_input_mask,
                            inputs_embeds=None,
                            labels=b_labels)
            loss = outputs[0]

            total_loss += loss.item()
            if args.debug:
                logger.info(total_loss)

            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.max_grad_norm)
            optimizer.step()
            scheduler.step()
            optimizer.zero_grad()

        avg_train_loss = total_loss / len(train_dataloader)

        logger.info("Average training loss: {0:.2f}".format(avg_train_loss))
        logger.info("Training epoch took: {:}".format(format_time(time.time() - t0)))
        logger.info("Running Validation...")

        t0 = time.time()
        model.eval()

        eval_loss, eval_accuracy = 0, 0
        nb_eval_steps, nb_eval_examples = 0, 0

        y_true = []
        y_pred = []
        label_map = int2tag
        logger.info("Label map: ", label_map)

        slot_preds = None
        out_slot_labels_ids = None

        for batch in val_dataloader:
            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            with torch.no_grad():
                outputs = model(b_input_ids,
                                token_type_ids=None,
                                attention_mask=b_input_mask)

            outputs = outputs[0]

            logits = torch.argmax(outputs, dim=2)
            logits = logits.detach().cpu().numpy()
            label_ids = b_labels.to('cpu').numpy()

            for i, label in enumerate(label_ids):
                # for index, sentence
                pred_label = []
                true_label = []
                for j, lb in enumerate(label):
                    # for index, label
                    if j == 0 and args.add_special_tokens:
                        # Vị trí đầu tiên trong câu
                        continue
                    elif lb == tag2int["-PAD-"]:
                        # Gặp vị trí cuối cùng

                        y_true.append(true_label)
                        y_pred.append(pred_label)
                        break
                    else:
                        # Các vị trí khác
                        true_label.append(label_map[lb])
                        pred_label.append(label_map[logits[i][j]])

                        if j == len(label) - 1:
                            y_true.append(true_label)
                            y_pred.append(pred_label)

        # print("Pred and true", y_pred, y_true)
        report = classification_report(y_true, y_pred, digits=4)

        # logger.info("\n%s", report)

        # print("  Acc score: {0:.2f}".format(eval_accuracy / nb_eval_steps))
        logger.info("Validation took: {:}".format(format_time(time.time() - t0)))

        f1_score_result = {
            "slot_precision": precision_score(y_true, y_pred),
            "slot_recall": recall_score(y_true, y_pred),
            "slot_f1": f1_score(y_true, y_pred)
        }

        logger.info("F1 result: ", f1_score_result)

        if best_result == 0 or best_result < f1_score_result['slot_f1']:
            if args.save_pretrained:
                output_dir = os.path.join(STORE_DIR,
                                        'checkpoint-{}-{}-{:.3f}'.format(model.__class__.__qualname__,
                                                                        args.learning_rate,
                                                                        f1_score_result['slot_f1']))
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                output_eval_file = os.path.join(output_dir, "eval_results.txt")
                with open(output_eval_file, "w") as writer:
                    writer.write(report)

                best_result = f1_score_result['slot_f1']

                print("Saving model to %s" % output_dir)

                with open(os.path.join(output_dir, "store_dict.json"), "w") as writer:
                    json.dump(model.store_dict, writer, indent=4)

                with open(os.path.join(output_dir, "label_mapping.json"), "w") as writer:
                    json.dump(tag2int, writer, indent=4)

                model_to_save = model.module if hasattr(model, 'module') else model
                model_to_save.save_pretrained(output_dir)
                tokenizer.save_pretrained(output_dir)
            
            logger.info("***** Eval results *****")
            logger.info("\n%s", report)
            best_report = report

    logger.info("\nTraining complete!\n*************************************************************************************************************************\n\n")
    with open(os.path.join(STORE_DIR, "best_report.txt"), "w") as writer:
        writer.write(best_report)

def categorical_accuracy(preds, y, tag_pad_idx):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """
    max_preds = preds.argmax(dim=1, keepdim=True)  # get the index of the max probability
    non_pad_elements = (y != tag_pad_idx).nonzero()
    correct = max_preds[non_pad_elements].squeeze(1).eq(y[non_pad_elements])
    # print(max_preds, non_pad_elements, correct, y)

    return correct.sum() / torch.FloatTensor([y[non_pad_elements].shape[0]]).to(device)


def format_time(elapsed):
    elapsed_rounded = int(round(elapsed))
    return str(datetime.timedelta(seconds=elapsed_rounded))


if __name__ == '__main__':
    main()
