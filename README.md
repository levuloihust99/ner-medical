# INSTALLATION GUIDE
## Requirements
- Python 3.8
### 1. Create virtual environment and install requirements
```shell
$ python -m venv .venv
$ source .venv/bin/activate
(.venv)$ pip install -U pip
(.venv)$ pip install wheel
(.venv)$ pip install -r requirements.txt
```

### 2. Install VnCoreNLP
```shell
$ mkdir -p vncorenlp/models/wordsegmenter
$ wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/VnCoreNLP-1.1.1.jar
$ wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/models/wordsegmenter/vi-vocab
$ wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/models/wordsegmenter/wordsegmenter.rdr
$ mv VnCoreNLP-1.1.1.jar vncorenlp/ 
$ mv vi-vocab vncorenlp/models/wordsegmenter/
$ mv wordsegmenter.rdr vncorenlp/models/wordsegmenter/
```

### 3. Install Java
```shell
$ sudo apt install default-jre
```

### 4. Train NER
Using `vinai/phobert-base`
```shell
$ python train.py --model-path pretrained/vinai/phobert-base
```
Using `NlpHUST/vibert4news-base-cased`
```shell
$ python train.py --model-path pretrained/NlpHUST/vibert4news-base-cased
```
Using `NlpHUST/vi-electra-base`
```shell
$ python train.py --model-path pretrained/NlpHUST/vi-electra-base
```
