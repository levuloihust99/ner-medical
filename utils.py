import nltk
import string


def load_data(path):
    raw = []
    tagged_sentences = []
    with open(path, 'r', encoding="utf-8") as lines:
        for line in lines:
            tmp_s = []
            sentences = line.strip()
            sentences = preprocess(sentences)
            raw.append(sentences)
            for w in sentences.split():
                if "_" not in w:
                    tmp_s.append((w, "S"))
                else:
                    tmp = w.split("_")
                    if len(tmp) == 2:
                        tmp_s.append((tmp[0], "B"))
                        tmp_s.append((tmp[1], "E"))
                    if len(tmp) > 2:
                        tmp_s.append((tmp[0], "B"))
                        for i in range(1, len(tmp) - 1):
                            tmp_s.append((tmp[i], "I"))
                        tmp_s.append((tmp[-1], "E"))
            tagged_sentences.append(tmp_s)
    return tagged_sentences, raw


def load_pos_data(path):
    raw = []
    tagged_sentences = []
    with open(path, 'r', encoding="utf-8") as lines:
        for line in lines:
            tmp_s = []
            sentences = line.strip()
            if len(sentences) == 0:
                continue
            raw.append(sentences)
            for e in sentences.split():
                if "//" in e:
                    w, t = "/", "CH"
                else:
                    w, t = "".join(e.split("/")[0:len(e.split("/")) - 1]), e.split("/")[-1]

                if "_" not in w:
                    tmp_s.append((w, "S", t))
                else:
                    tmp = w.split("_")
                    if len(tmp) == 2:
                        tmp_s.append((tmp[0], "B", t))
                        tmp_s.append((tmp[1], "E", "-SUB-"))
                    if len(tmp) > 2:
                        tmp_s.append((tmp[0], "B", t))
                        for i in range(1, len(tmp) - 1):
                            tmp_s.append((tmp[i], "I", "-SUB-"))
                        tmp_s.append((tmp[-1], "E", "-SUB-"))
            tagged_sentences.append(tmp_s)
    return tagged_sentences, raw


def load_postag_data(path):
    raw = []
    tagged_sentences = []
    with open(path, 'r', encoding="utf-8") as lines:
        for line in lines:
            tmp_s = []
            sentences = line.strip()
            if len(sentences) == 0:
                continue
            raw.append(sentences)
            for e in sentences.split():
                if "//" in e:
                    w, t = "/", "CH"
                else:
                    w, t = "".join(e.split("/")[0:len(e.split("/")) - 1]), e.split("/")[-1]

                if "_" not in w:
                    tmp_s.append((w, "B_" + t))
                else:
                    tmp = w.split("_")
                    if len(tmp) == 2:
                        tmp_s.append((tmp[0], "B_" + t))
                        tmp_s.append((tmp[1], "I_" + t))
                    if len(tmp) > 2:
                        tmp_s.append((tmp[0], "B_" + t))
                        for i in range(1, len(tmp)):
                            tmp_s.append((tmp[i], "I_" + t))
            tagged_sentences.append(tmp_s)
    return tagged_sentences, raw


# Some usefull functions
def tag_sequence(sentences, tag2int):
    return [[tag2int[t] for w, t in sentence] for sentence in sentences]


def text_sequence(sentences):
    return [[w for w, t in sentence] for sentence in sentences]


def tag1_sequence_pos(sentences, tag2int):
    return [[tag2int[t1] for w, t1, t2 in sentence] for sentence in sentences]


def tag2_sequence_pos(sentences, tag2int):
    return [[tag2int[t2] for w, t1, t2 in sentence] for sentence in sentences]


def text_sequence_pos(sentences):
    return [[w for w, t1, t2 in sentence] for sentence in sentences]


def preprocess(text):
    text = " ".join(nltk.word_tokenize(text))
    return text


def load_ner_data(path):
    raw = []
    tagged_sentences = []
    with open(path, 'r', encoding="utf-8") as lines:
        for line in lines:
            tmp_s = []
            sentences = line.strip()
            raw.append(sentences)
            if len(sentences) == 0:
                continue
            for w in sentences.split():
                tmp = w.split("/")
                word = "".join(tmp[0:-1])
                if word == "":
                    word = "/"
                lb = tmp[-1]
                tmp_s.append((word, lb))
            tagged_sentences.append(tmp_s)

    return tagged_sentences, raw


def load_medical_ner_data(path):
    """Process the data source, split train and test set. Each set is a list of sentence.
    Each sentence is a sequence of tuple (<word>, <label>).

    Example: [('Đinh', 'B-ten_cay'), ('lăng', 'I-ten_cay'), ('là', 'O'), ('một', 'O'), ('cây', 'O'), ('thuốc', 'O')]
    """
    raw = []
    tagged_sentences = []
    f = open("final_dataset.txt", "w", encoding="utf-8")
    # tmp_word = ["", ""]
    with open(path, 'r', encoding="utf-8") as lines:
        tmp_sentences = ""
        tmp_set_word_label = []
        is_beginning_sentence = True
        count_dataset = {}
        count_error = 0
        flag_error = False
        tmp_line_true = []
        for line in lines:
            tmp_line_true.append(line)
            # print("1. Line", line)
            if line == "\n" or line.find("-DOCSTART- -X- -X- O") > -1:
                if len(tmp_set_word_label) > 0:
                    if not flag_error:
                        raw.append(tmp_sentences)
                        tagged_sentences.append(tmp_set_word_label)
                        for token in tmp_line_true:
                            f.write(token)
                    else:
                        flag_error = False
                    tmp_line_true = []
                    is_beginning_sentence = True
                    # print("Final", tmp_sentences, tmp_set_word_label)
                    tmp_sentences = ""
                    tmp_set_word_label = []
                continue
            else:
                line = line.replace("\n", "")
                tmp = line.split(" ", 1)
                word = "".join(tmp[0:-1])

                if word == "":
                    word = "/"
                if word in string.punctuation:
                    tmp_line_true.pop()
                    continue

                tmp_sentences += ("" if is_beginning_sentence else " ") + line.replace(" ", "/").lower()
                lb = tmp[-1].replace("_ _ ", "")
                if lb.find('co_the') < 0 and lb.find('nguyen_nhan_benh') < 0 and lb.find('ten_benh') < 0 and lb.find(
                        'doi_tuong_nguy_co') < 0 \
                        and lb.find('trieu_chung_benh') < 0 and lb.find('ten_thuoc') < 0 and lb.find(
                    'ten_cay') < 0 and lb.find('dieu_tri') < 0 \
                        and lb.find('chan_doan') < 0 and lb.find('tuoi_benh_nhan') < 0:
                    lb = 'O'
                    # if tmp_word[0] != "":
                    #     if tmp_word[0] not in data_for_each_label.keys():
                    #         data_for_each_label[tmp_word[0]] = [tmp_word[1]]
                    #     else:
                    #         data_for_each_label[tmp_word[0]].append(tmp_word[1])
                    #         tmp_word = ["", ""]
                else:
                    if lb.find('B') > -1:
                        # tmp_word[0] = lb
                        # tmp_word[1] = word
                        if lb not in count_dataset.keys():
                            count_dataset[lb] = 1
                        else:
                            count_dataset[lb] += 1
                    else:
                        # print("Label: ", lb)
                        # print(tmp_set_word_label)
                        if len(tmp_set_word_label) == 0 or tmp_set_word_label[-1][1].find('O') > -1:
                            print("ERROR", line, tmp_set_word_label)
                            flag_error = True
                            count_error += 1
                        # else:
                        #     tmp_word[1] += " " + word

                    is_beginning_sentence = False

                tmp_set_word_label.append((word.lower(), lb))

    f.close()

    # for k, v in data_for_each_label.items():
    #     print("Key, value: ", k, v)

    from sklearn.model_selection import train_test_split
    raw_train, raw_dev, train_sentences, dev_sentences = train_test_split(raw, tagged_sentences, test_size=0.1,
                                                                          random_state=42)
    # print(len(raw), len(tagged_sentences))
    print("count dataset", count_dataset)
    print("count error", count_error)
    print("LEN Dataset: ", len(train_sentences), len(raw_train), len(dev_sentences), len(raw_dev))
    # print(train_sentences[150], raw_train[150], dev_sentences[340], raw_dev[340])
    return train_sentences, raw_train, dev_sentences, raw_dev


def text_sequence_ner(sentences):
    return [[w for w, t1 in sentence] for sentence in sentences]
