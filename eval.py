import torch
from torch.utils.data import DataLoader, TensorDataset
from transformers import (
    PhobertTokenizer,
    RobertaForTokenClassification,
    BertTokenizer
)
from transformers.tokenization_electra import ElectraTokenizer
from models import (
    BertPosTagger,
    BertPosTaggerElectra,
    PhoBertPosTagger,
)
from seqeval.metrics import classification_report
from tensorflow.keras.preprocessing.sequence import pad_sequences

import logging
import pickle
import json
import time
import datetime
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--model-path',
                    help='Path to model to be evaluated')
parser.add_argument('--log', type=bool, default=False,
                    help='Whether to log evaluation results to file. Default to True')

args = parser.parse_args()
with open(os.path.join(args.model_path, "store_dict.json"), "r") as reader:
    model_args = json.load(reader)
model_args = argparse.Namespace(**model_args)


def format_time(elapsed):
    elapsed_rounded = int(round(elapsed))
    return str(datetime.timedelta(seconds=elapsed_rounded))


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

MODEL_PATH = args.model_path
MAX_SEQUENCE_LENGTH = 256

with open(os.path.join(MODEL_PATH, 'label_mapping.json'), 'r') as reader:
    tag2int = json.load(reader)
int2tag = {v: k for k, v in tag2int.items()}

if MODEL_PATH.find('vinai/phobert-base') > -1:
    tokenizer = PhobertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    model = PhoBertPosTagger.from_pretrained(MODEL_PATH, args=model_args)

elif MODEL_PATH.find('NlpHUST/vibert4news-base-cased') > -1:
    tokenizer = BertTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    model = BertPosTagger.from_pretrained(MODEL_PATH, args=model_args)

elif MODEL_PATH.find('NlpHUST/vi-electra-base') > -1:
    tokenizer = ElectraTokenizer.from_pretrained(MODEL_PATH, do_lower_case=False, tokenize_chinese_chars=False)
    model = BertPosTaggerElectra.from_pretrained(MODEL_PATH, args=model_args)
    
model.eval()
"""
Setup CUDA
"""
if torch.cuda.is_available():
    model.cuda()
    device = torch.device("cuda")
    logger.info('There are %d GPU(s) available.' % torch.cuda.device_count())
    logger.info('We will use the GPU:{}, {}'.format(torch.cuda.get_device_name(0), torch.cuda.get_device_capability(0)))
else:
    logger.info('No GPU available, using the CPU instead.')
    device = torch.device("cpu")

if not model_args.vlsp:
    with open("data/medical/dev_sentences.pkl", 'rb') as f:
        eval_data = pickle.load(f)
else:
    # <VLSP-2018>
    # data_dir = "data/VLSP-2018-NER"
    reader = open("data/VLSP-2018-NER/dev.txt", 'r')
    data = reader.read()
    sentences = data.split('\n\n')[:-1]
    words = [sentence.split('\n') for sentence in sentences]
    eval_data = [[tuple(word.split()[:2]) for word in sentence] for sentence in words]
    # </VLSP-2018>

if MODEL_PATH.find("vinai/phobert-base") > -1:
    from vncorenlp import VnCoreNLP
    rdrsegmenter = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')
    from data_utils import segment_word

    eval_data = segment_word(eval_data, rdrsegmenter)
    

eval_input_ids = []
eval_label_ids = []
error_count = 0

for sent in eval_data:
    words, tags = zip(*sent)

    raw_sent = " ".join(words)
    encoded_sent = tokenizer.encode(raw_sent, add_special_tokens=True)

    tmp_labels = [0]
    for i, word in enumerate(words):
        tokens = tokenizer.tokenize(word)
        label = tags[i]
        for token in tokens:
            tmp_labels.append(tag2int[label])
    tmp_labels.append(0)

    if len(encoded_sent) != len(tmp_labels):
        error_count += 1
        continue

    eval_input_ids.append(encoded_sent)
    eval_label_ids.append(tmp_labels)

# Padding to max length
eval_input_ids = pad_sequences(eval_input_ids, maxlen=MAX_SEQUENCE_LENGTH,
                               dtype='long', value=tokenizer.pad_token_id,
                               truncating='post', padding='post')
eval_label_ids = pad_sequences(eval_label_ids, maxlen=MAX_SEQUENCE_LENGTH,
                               dtype='long', value=tokenizer.pad_token_id,
                               truncating='post', padding='post')

eval_masks = []

for sent in eval_input_ids:
    attention_mask = [int(token != tokenizer.pad_token_id) for token in sent]
    eval_masks.append(attention_mask)

eval_input_ids = torch.tensor(eval_input_ids, dtype=torch.long)
eval_masks = torch.tensor(eval_masks, dtype=torch.long)
eval_label_ids = torch.tensor(eval_label_ids, dtype=torch.long)

dataset = TensorDataset(eval_input_ids, eval_masks, eval_label_ids)
dataloader = DataLoader(dataset, batch_size=16, shuffle=False)

# Run validation
start_time = time.time()
model.eval()

y_true = []
y_pred = []
for batch in dataloader:
    batch = tuple(t.to(device) for t in batch)
    input_ids, eval_masks, label_ids = batch

    with torch.no_grad():
        outputs = model(
            input_ids=input_ids,
            attention_mask=eval_masks,
            labels=label_ids
        )

    logits = outputs[1]

    predicts = torch.argmax(logits, dim=-1)

    label_ids = label_ids.cpu().numpy()
    predicts = predicts.cpu().numpy()
    for i, sent_labels in enumerate(label_ids):
        true_labels = []
        pred_labels = []

        for j, lb in enumerate(sent_labels):
            if j == 0 and model_args.add_special_tokens:
                continue
            elif lb == 0:
                y_true.append(true_labels)
                y_pred.append(pred_labels)
                break
            else:
                true_labels.append(int2tag[lb])
                pred_labels.append(int2tag[predicts[i][j]])
                if j == len(sent_labels) - 1:
                    y_true.append(true_labels)
                    y_pred.append(pred_labels)

if args.log:
    f_log = open("visualize.txt", "w")
    f_err = open("error_visualize.txt", "w")
    err = 0
    for i in range(len(y_true)):
        sentence_no = "Sentence No.{}\n".format(i)
        words, labels = zip(*eval_data[i])
        display_words = ['|' + word + (22 - len(word)) * " " for word in words[:len(y_true[i])]]
        display_words = "".join(display_words)
        sentence_out = "Sentence:{}{}\n".format(" " * 11, display_words)

        display_ground_truth = "".join(['|' + lb + (22 - len(lb)) * " " for lb in y_true[i]])
        ground_truth_out = "Ground truth:{}{}\n".format(" " * 7, display_ground_truth)

        display_predict = "".join(['|' + lb + (22 - len(lb)) * " " for lb in y_pred[i]])
        predict_out = "Predict:{}{}\n".format(" " * 12, display_predict)
        seperate = "-" * len(display_ground_truth) + '\n'

        all_equals = all([x == y for x, y in zip(y_true[i], y_pred[i])])
        f_log.write("{}{}{}{}{}".format(sentence_no, sentence_out, ground_truth_out, predict_out, seperate))
        if not all_equals:
            err += 1
            f_err.write("{}{}{}{}Count = {}\n{}".format(sentence_no, sentence_out, ground_truth_out, predict_out, err,
                                                        seperate))

    f_log.close()
    f_err.close()

report = classification_report(y_true, y_pred)
print("Validation took: {}".format(format_time(time.time() - start_time)))
logger.info("***** Eval results *****")
logger.info("\n{}".format(report))
